var webPage = require('webpage');
var sys = require('system');
var fs = require('fs');
var page = webPage.create();
var id = sys.args[1];

page.zoomFactor = 2.5;

var tweet = {
	date:sys.args[2],
	name:JSON.parse(sys.args[3]),
	screenName:sys.args[4],
	verified:sys.args[5],
	avatar:sys.args[6],
	tweetText:JSON.parse(sys.args[7]),
	imageUrl:sys.args[8].replace(/^'|'$/g, ""), //ESCAPING FOR COMMAND LINE LEAVES QUOTES
	retweets:sys.args[9],
	favorites:sys.args[10],
	retweetImages:sys.args[11],
	hiliteStart:parseInt(sys.args[12]),
	hiliteEnd:parseInt(sys.args[13])
};

tweet.tweetText = tweet.tweetText.replace(/https:\/\/t\.co\/\S+/g, "");
page.open('http://localhost/apps/twitter_snapper/renderer_still/template/template.html', function(status) {
	page.evaluate(function(tweet) {
		populateTweet(tweet);
	}, tweet);
	setTimeout(function(){
		page.viewportSize = {
		  width: 1920,
		  height: 1
		};
		var imagePath = '/home/cnnitouch/www/cnnimages/';
		var filename = 'twitter_snapper_' + id + '.png';
		page.render( imagePath + filename );

		if (tweet.hiliteEnd){
			page.evaluate(function(){
				setHilite();
			});
			var hiliteFilename = 'twitter_snapper_' + id + '_hilite_' + tweet.hiliteStart + '_' + tweet.hiliteEnd + '.png';
			page.render( imagePath + hiliteFilename );
			console.log(hiliteFilename);
		} else {
			console.log(filename);
		}

		var measure = page.evaluate(function(){
			return measureTweet();
		});
		var dataFilename = 'tweet_new_' + id + '.json'; 
		fs.write('/mnt/data/tmp/twitter_snapper_data/' + dataFilename, JSON.stringify(measure), 'w');

		phantom.exit();
	}, 3000);
});
