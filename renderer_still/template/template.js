"use strict";
ready( function(){

});
var tweet;
function populateTweet( t ){
	tweet = t;
	var blurUrl = "http://cnnitouch/utilities/blur_image.php?img=";
	//document.querySelectorAll('body')[0].style.backgroundImage = 'url(' + tweet.backgroundImage + ')';
	document.getElementById('realName').innerHTML = tweet.name;
	document.getElementById('screenName').innerHTML = '@' + tweet.screenName;
	document.getElementById('avatar').src = tweet.avatar;
	document.getElementById('tweetText').innerHTML = twemoji.parse(spanWrap(tweet.tweetText));

	var img = document.getElementById('tweetImg').querySelectorAll('img')[0];

	if (tweet.imageUrl.length > 2){
		img.src = tweet.imageUrl;
	} else {
		img.style.display = 'none';
	}
	if (img.clientWidth > 800){
		img.style.width = '800px';
		img.style.height = 'auto';
	}
	document.getElementById('retweets').querySelectorAll('.dataNumber')[0].innerHTML = numberWithCommas(tweet.retweets);
	document.getElementById('favorites').querySelectorAll('.dataNumber')[0].innerHTML = numberWithCommas(tweet.favorites);
	if (tweet.verified == "1") {
		//document.getElementById('verified').style.display = 'inherit';
	} else {
		document.getElementById('verified').style.display = 'none';
	}
	var time = new Date(tweet.date);
	var minutes = time.getUTCMinutes().toString();
	if ( minutes.length == 1 ){
		minutes = "0" + minutes;
	}
	var timeString = time.getUTCHours().toString() + ':' + minutes + ' UTC - ' +
					 time.getUTCDate().toString() + ' ' + months[time.getUTCMonth()] + ' ' + time.getUTCFullYear().toString();
	document.getElementById('time').innerHTML=timeString;

	document.getElementById('retweetIcons').innerHTML = "";
	var retweetImages = tweet.retweetImages.split(',');
	for (var img in retweetImages){
		var imageElement  = document.createElement('img');
		imageElement.src = blurUrl + retweetImages[img];
		document.getElementById('retweetIcons').appendChild(imageElement);	
	}
}

function htmlDecode(input){
	var e = document.createElement('div');
	e.innerHTML = input;
	return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function setHilite(){
	var text = htmlDecode(tweet.tweetText);
	var hiliteStart = parseInt(tweet.hiliteStart);
	var hiliteEnd = parseInt(tweet.hiliteEnd);
	var html = text.substring(0, hiliteStart);
	html += "<span class='hilite'>";
	html += text.substring(hiliteStart, hiliteEnd);
	html += "</span>";
	html += text.substring(hiliteEnd);
	document.getElementById('tweetText').innerHTML = twemoji.parse( html );
}

function measureTweet(){
	var hasHilite = tweet.hiliteEnd ? true : false;
	if ( hasHilite ) {
		var tweetText = htmlDecode(tweet.tweetText);
		var preHilite = tweetText.substring(0, parseInt(tweet.hiliteStart));
		var hilite = tweetText.substring( parseInt(tweet.hiliteStart), parseInt(tweet.hiliteEnd));
		var postHilite = tweetText.substring(parseInt(tweet.hiliteEnd));
		var displayText = twemoji.parse(preHilite) + twemoji.parse(spanWrap(hilite)) + twemoji.parse(postHilite);
		document.getElementById('tweetText').innerHTML = displayText;
	} else {
		document.getElementById('tweetText').innerHTML = twemoji.parse(spanWrap(tweet.tweetText));
	}
	var words = document.getElementsByClassName('tweetWord');
	words = [].slice.call(words);
	var measurementData = words.map( function(word){
		var rect = word.getBoundingClientRect();
		var data = {
			word:word.innerHTML,
		    top: rect.top,
			right: rect.right,
			bottom: rect.bottom,
			left: rect.left,
			width: rect.width
		};
		return data;
	});
	return {hilite:hasHilite, measurements:measurementData};
}

function ready(fn) {
	if (document.readyState != 'loading'){
  		fn();
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
}

function spanWrap(text){
	var arr = text.split(' ');
	var ret = "";
	for (var i = 0; i < arr.length; i++){
		ret += "<span class='tweetWord'>" + arr[i] + "</span> ";
	}
	return ret;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
