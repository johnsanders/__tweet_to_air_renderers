<?php
	class TwitterApiHelpers {
		public function __construct(){
			require_once('TwitterAPIExchange.php');
			$settings = (array)json_decode( file_get_contents('twitter_api_credentials.json') );
			$this->twitter = new TwitterAPIExchange($settings);
		}
		public function getTweetData($id) {
			$url = 'https://api.twitter.com/1.1/statuses/lookup.json';
			$getField = '?id=' . $id . '&tweet_mode=extended';
			$requestMethod = 'GET';
			@$raw = $this->twitter->setGetfield($getField)
							->buildOauth($url, $requestMethod)
							->performRequest();
			$tweet = json_decode($raw);

			if ( isSet($tweet->errors) && count($tweet->errors) > 0 ){
				$ret = array();
				$ret['status'] = 'error';
				echo json_encode($ret);
				exit;
			}
			if ( count($tweet) == 1 ){
				return $tweet[0];
			} else {
				return $tweet;
			}
		}
		public function replaceShortenedLinks($tweet) {
			$text = $tweet->full_text;
			foreach ($tweet->entities->urls as $url){
				$text = str_replace( $url->url, $url->display_url, $text );
			}
			return $text;
		}

		public function getRetweeters($id){
			$requestMethod='GET';
			$url = 'https://api.twitter.com/1.1/statuses/retweeters/ids.json';
			$getField = '?stringify_ids=true&id=' . $id;

			@$raw = $this->twitter->setGetField($getField)
					->buildOauth($url, $requestMethod)
					->performRequest();
			$retweeters = json_decode($raw)->ids;
			if ( count($retweeters >= 11) ){
				$maxRetweets = 11;
			} else {
				$maxRetweets = count($retweeters);
			}
		
			$url = 'https://api.twitter.com/1.1/users/lookup.json';
			$getField = '?user_id=';

			for ($i=0; $i<$maxRetweets-1; $i++){
				if ($i>0){
					$getField .= ',';
				}
				if (isSet($retweeters[$i])){
					$getField .= $retweeters[$i];
				}
			}
			@$raw = $this->twitter->setGetField($getField)
				->buildOAuth($url, $requestMethod)
				->performRequest();
			$retweeters = json_decode($raw);
			
			$retweetImages = '';
			if ( gettype($retweeters) == 'array') {
				for ($i=0; $i<count($retweeters); $i++){
					if ($i>0){
						$retweetImages .= ',';
					}
						$retweetImages .= $retweeters[$i]->profile_image_url_https;
				}
			}
			return $retweetImages;
		}
		public static function getImageUrl($tweet) {
			$imageUrl = "''";
			if ( isSet($tweet->entities->media) && count($tweet->entities->media) > 0){
				$imageUrl = escapeshellarg($tweet->entities->media[0]->media_url);
			} else {
				// IF THERE'S NO IMAGE ATTACHED,
				//LET'S CHECK IF THERE'S A LINK IN THE TWEET AND SEE IF THERE'S A TWITTER IMAGE SPECIFIED IN THAT PAGE'S META TAGS
				//EASIER TO JUST REGEX IT THAN FIGHT THROUGH PARSING THE WHOLE HTML PAGE
				$pattern = '/\<meta\s*(?:property|name)\s*=\s*"twitter:(?:image|image:src)"\s*content\s*=\s*"(.*)".*\>/';
				if ( isSet($tweet->entities->urls) && count($tweet->entities->urls) > 0){
					$urls = $tweet->entities->urls;
					for ($i=0; $i<count($urls); $i++){
						$page = @file_get_contents( $urls[$i]->expanded_url );
						preg_match($pattern, $page, $matches);
						if (count($matches) > 0){
							$imageUrl = $matches[1];
							break;
						}
					}
				}
			}
			return $imageUrl;
		} 
	}
?>
