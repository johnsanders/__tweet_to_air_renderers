<?php
	require('TwitterApiHelpers.php');
	require('delete_old_tweet_measurement_data.php');
	require('TwitterRenderHelpers.php');
	$twitterApiHelpers = new TwitterApiHelpers();
	$imagePath = '/home/cnnitouch/www/cnnimages/';

	if (isSet($_GET['id'])){
		$id = $_GET['id'];
	} else {
		$id = $argv[1];
	}
	if (isSet($_GET['savePath'])){
		$savePath = $_GET['savePath'] . '/';
	} else {
		$savePath = "";
	}
	if (isSet($_GET['hiliteStart'])){
		$hiliteStart = $_GET['hiliteStart'];
	} else {
		$hiliteStart = "";
	}
	if (isSet($_GET['hiliteEnd'])){
		$hiliteEnd = $_GET['hiliteEnd'];
	} else {
		$hiliteEnd = "";
	}
	$tweet = $twitterApiHelpers->getTweetData($id);
	$tweet->full_text = $twitterApiHelpers->replaceShortenedLinks($tweet);
	//var_dump($tweet);
	//exit;
	if ( isSet($tweet->errors) && count($tweet->errors) > 0 ){
		$ret = array();
		$ret['status'] = 'error';
		echo json_encode($ret);
		exit;
	}

	$imageUrl = TwitterApiHelpers::getImageUrl($tweet);
	$retweetImages = $twitterApiHelpers->getRetweeters($id);
	$tweetFilename = TwitterRenderHelpers::renderTweet( $tweet, $imageUrl, $retweetImages, $hiliteStart, $hiliteEnd );
	TwitterRenderHelpers::renderProfileBackground($tweet);

	if ($hiliteEnd) {
		$newFilename = 'tweet_' . $tweet->user->screen_name . '_' . $tweet->id_str . '_hilite_' . $hiliteStart . '_' . $hiliteEnd . '.png';
	} else {
		$newFilename = 'tweet_' . $tweet->user->screen_name . '_' . $tweet->id_str . '.png';
	}

	TwitterRenderHelpers::renderComposite($imagePath, $tweetFilename, $id, $newFilename, $savePath);

	$ret = array( "status" => "success",
				  "filename" => $newFilename,
				  "tweetId" => $tweet->id_str,
				  "twitterUser" => $tweet->user->screen_name,
				  "tweetText" => $tweet->full_text,
				  "date" => $tweet->created_at
				  );
	echo json_encode($ret);
?>
