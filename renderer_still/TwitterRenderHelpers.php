<?php
	class TwitterRenderHelpers {
		/*public static function scaleImage( $im, $maxWidth, $maxHeight, $minAspect, $maxAspect ) {
			//RESIZE TO PREFERRED SIZE WHILE MAINTAINING ASPECT
			$size = $im->getImageGeometry();
			$width = $size['width'];
			$height = $size['height'];
			$aspect= $width / $height;
			if ($aspect > $maxAspect) {
				$newWidth = $maxWidth;
				$newHeight = $newWidth / $aspect;
			} else if ($aspect < $minAspect) {
				$newHeight = $maxHeight;
				$newWidth = $newHeight * $aspect;
			} else {
				$newHeight = $height;
				$newWidth = $width;
			}
			$im -> scaleImage($newWidth, $newHeight);
			return $im;
		}*/		

		function scaleAndMaintain( $im, $maxWidth, $maxHeight, $minAspect, $maxAspect ) {
			//$im = scaleImage( $im, $maxWidth, $maxHeight, $minAspect, $maxAspect );
			$size = $im->getImageGeometry();
			$width = $size['width'];
			$height = $size['height'];
			$aspect = $width / $height;

			if ( $width > $height && $height < 1000 ) {
				$width = 1920 * .8;
				$height = $width / $aspect;
			} else {
				$height = 1080 * .95;
				$width = $height * $aspect;

			}

			$im -> scaleImage($width, $height);

			//IF NECESSARY, ADD BLURRY BKG FOR VERTICAL IMAGES
			//if ( $aspect < $minAspect ) {
			//	$im = handleVertical($im, $maxWidth, $maxHeight);
			//}
			//IF IT'S WIDER THAN MAXASPECT ADD A BLUR LETTERBOX
			//else if ($aspect > $maxAspect) {
			//	$im = handleHorizontal($im, $maxWidth, $maxHeight);
			//} else {
			//	$im -> scaleImage($maxWidth, $maxHeight);
			//}
			return $im;
		}
		public static function renderProfileBackground($tweet){

			$basePhantomProfile = '/home/cnnitouch/phantomjs-2.1.1/bin/phantomjs /home/cnnitouch/phantomjs/phantom-multicap.js';
			$id = $tweet->id;
			$profileImagePath = "/home/cnnitouch/www/cnnimages/twitter_snapper_${id}_profile.png";
			$screenName = escapeshellarg($tweet->user->screen_name);

			exec( $basePhantomProfile . " http://twitter.com/$screenName $profileImagePath 1 2 2880 1620 0 0 2880 1620" );

			//	BLUR THE PROFILE IMAGE A BIT JUST IN CASE THERE'S SOMETHING NASTY IN THERE..
			$profileImg = new Imagick( $profileImagePath );
			$profileImg->blurImage(0,8);
			$profileImg->writeImage( $profileImagePath );
		}	

		public static function renderTweet( $tweet, $imageUrl, $retweetImages, $hiliteStart, $hiliteEnd ) {
			$basePhantomTweet = '/home/cnnitouch/phantomjs-2.1.1/bin/phantomjs /home/cnnitouch/www/apps/twitter_snapper/renderer_still/phantom_snapper.js';
			$id = strval($tweet->id);
			$date = escapeshellarg($tweet->created_at);
			$tweetText = escapeshellarg(json_encode($tweet->full_text));
			$retweets = escapeshellarg($tweet->retweet_count);
			$retweetImages = escapeshellarg( $retweetImages );
			$favorites = escapeshellarg($tweet->favorite_count);
			$name = escapeshellarg(json_encode($tweet->user->name));
			$screenName = escapeshellarg($tweet->user->screen_name);
			$verified = escapeshellarg($tweet->user->verified);
			$avatar = escapeshellarg($tweet->user->profile_image_url);
			$background = escapeshellarg($tweet->user->profile_background_image_url);
			$imageUrl = escapeshellarg($imageUrl);
			$time = new DateTime($tweet->created_at);
			exec( "$basePhantomTweet $id $date $name $screenName $verified $avatar $tweetText $imageUrl $retweets $favorites $retweetImages $hiliteStart $hiliteEnd", $out);
			return $out[0];
		}


		public static function renderComposite($imagePath, $oldFilename, $id, $newFilename, $savePath) {
			$tweetImg = new Imagick( $imagePath . $oldFilename );
			$tweetImg = TwitterRenderHelpers::scaleAndMaintain( $tweetImg, 1920, 1080, 1.6, 1.9 );
			//unlink($imagePath . $oldFilename);

			$size = $tweetImg->getImageGeometry();
			$width = $size['width'];
			$height = $size['height'];

			// MAKE A DROP SHADOW FOR THAT GUY
			$shadowBase = new Imagick();
			$shadowBase -> newImage($width, $height, new ImagickPixel('rgba(0%,0%,0%,0.6)'));
			$shadow = new Imagick();
			$shadow -> newImage(1920, 1080, 'none');
			$shadow->compositeImage( $shadowBase, Imagick::COMPOSITE_OVER, 1920/2 - $width/2, 1080/2 - $height/2 );
			$shadow -> blurImage(80, 80, Imagick::CHANNEL_ALL);


			$profileImg = new Imagick( $imagePath . 'twitter_snapper_' . $id . '_profile.png' );
			$profileImg->scaleImage( 1920, 1080 );
			$profileImg->colorizeImage(new ImagickPixel('#333333'), new ImagickPixel("rgba(60%,60%,60%,1.0)"));

			$profileImg->compositeImage($shadow, Imagick::COMPOSITE_OVER, 0,0);
			$profileImg->compositeImage($tweetImg, Imagick::COMPOSITE_OVER, (1920 - $width)/2, (1080-$height) / 2);

			$profileImg->writeImage( $imagePath . $newFilename );
			if( $savePath != "" ) {
				$profileImg->writeImage( $savePath . $newFilename );
			}
		}
	}
?>
