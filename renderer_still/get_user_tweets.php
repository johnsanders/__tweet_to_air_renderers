<?php
	require_once('TwitterAPIExchange.php');
	function getUserTweets($user, $sinceId){
		$settings = (array)json_decode( file_get_contents('/home/cnnitouch/www/apps/twitter_snapper/renderer_still/twitter_api_credentials.json') );
		$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
		$getField = '?screen_name=' . $user .
					'&exclude_replies=false' .
					'&include_rts=true' .
					'&since_id=' . $sinceId;
		$requestMethod = 'GET';
		$twitter = new TwitterAPIExchange($settings);
		@$raw = $twitter->setGetfield($getField)
						->buildOauth($url, $requestMethod)
						->performRequest();
		$tweet = json_decode($raw);

		if ( isSet($tweet->errors) && count($tweet->errors) > 0 ){
			$ret = array();
			$ret['status'] = 'error';
			echo json_encode($ret);
			exit;
		}
		return $tweet;
	}
?>
