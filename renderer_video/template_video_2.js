'use strict';
var bkg, tweet, renderer, minScale, maxScale;
var minScalePortrait = .9;
var maxScalePortrait = 1.1;
var minScaleLandscape = .7;
var maxScaleLandscape = .9;
var tweenData = {blur:0, brightness:1, scale:null, rotation:0};
var tl = new TimelineMax();
var itemsLoaded = 0;
var coverLines = [];
var hilite, wordMeasurements, lineMeasurements;
var revealSecondsPerPixel = .001;
var fullDuration = 10;

function createTimeline(){
	tl.from(bkg, 1.5, {scale:1.5}, 0);
	tl.add(TweenLite.from( tweenData, 1.5, {blur:30, brightness:3, onUpdate:setBkgParams} ), 0);
	tl.to(bkg, 1.5, {opacity:.5}, 1);
	tl.to(tweet, 0.75, {opacity:1}, 1);
	tl.from(tweenData, 0.75, {rotation:30}, 1);
	tl.add(TweenLite.to(tweenData, 9, {scale:maxScale, onUpdate:setTweetParams}), 1);
	var delay = 0; 
	for (var i=0; i<coverLines.length; i++){
		var dur = revealSecondsPerPixel * coverLines[i].dataset.width; 
		if (hilite){
			tl.from( coverLines[i], dur, {width:0}, 1.5 + delay );
		} else {
			tl.to( coverLines[i], dur, {width:0, left:coverLines[i].dataset.right}, 1 + delay ); 
		}
		if (hilite){
			delay += dur;
		} else {
			delay += dur - .2; 
		}
	}
	tl.pause();
}
function setBkgParams(){
	bkg.style.filter = 'blur(' + tweenData.blur.toString() + 'px) brightness(' + tweenData.brightness.toString() + ')';
}
function setTweetParams(){
	tweet.style.transform = 'scale(' + tweenData.scale.toString() + ') rotateY(' + tweenData.rotation.toString() + 'deg)';
}
function loadContent(id, ignore){
	return new Promise( (resolve, reject) => { 
		document.getElementById('bkg').src = '/cnnimages/twitter_snapper_' + id + '_profile.png';
		document.getElementById('tweetImg').src = '/cnnimages/twitter_snapper_' + id + '.png';
		document.getElementById('bkg').addEventListener("load", itemLoaded );
		document.getElementById('tweetImg').addEventListener("load", itemLoaded );
		var request = new XMLHttpRequest();
		request.open('GET', '/apps/twitter_snapper/data/tweet_new_' + id + '.json', true);
		request.onload = function() {
			try {
				var data = JSON.parse(this.response);
				hilite = data.hilite;
				wordMeasurements = data.measurements; 
				lineMeasurements = measureLines(wordMeasurements);
				itemLoaded();
			} catch (err) {
			}
			setTimeout( resolve, 2000 );
		};
		request.send();
	});
}

function itemLoaded(){
	itemsLoaded++;
	if (itemsLoaded == 3) {
		stageAnimation();
	}
}
function stageAnimation(){
	bkg = document.getElementById('bkg');
	tweet = document.getElementById('tweet');
	var tweetImg = document.getElementById('tweetImg');
	var tweetAspect = tweetImg.clientWidth / tweetImg.clientHeight;
	var imgWidth, imgHeight, verticalOffset;
	var imgOrigWidth = tweetImg.clientWidth;
	if (tweetImg.clientWidth > tweetImg.clientHeight && 1700/tweetAspect < 1300) {
		imgWidth = 1700;
		imgHeight = 1700/tweetAspect;
		minScale = minScaleLandscape;
		maxScale = maxScaleLandscape;
		verticalOffset = -100;
	} else {
		imgWidth = 980;
		imgHeight = 980/tweetAspect;
		minScale = minScalePortrait;
		maxScale = maxScalePortrait;
		verticalOffset = 0;
	}
	tweenData.scale = minScale;
	tweetImg.style.width = px(imgWidth);
	tweetImg.style.height = px(imgHeight);
	tweet.style.top = px((1080 - imgHeight)/2) + verticalOffset;
	tweet.style.left = px((1920 - imgWidth)/2);

	var coverDivContainer = document.getElementById('coverDivContainer');
	var scale = imgWidth / imgOrigWidth;
	coverDivContainer.style.transform = "scale(" + scale.toString() + ")";
	for (var line in lineMeasurements) {
		var coverDiv = createCoverDiv(lineMeasurements[line]); 
		coverDivContainer.appendChild(coverDiv);
		coverLines.push(coverDiv);
	}
	if (hilite) {
		document.getElementById('coverDivContainer').classList.add('blendMultiply');
	}
	reset();
	createTimeline();
}
function createCoverDiv(line) {
	var scale = 2.5;
	var coverDiv = document.createElement('div');
	var width = ((line.right - line.left) * scale) + 20;
	coverDiv.style.top = px(line.top * scale); 
	coverDiv.style.left = px((line.left * scale) - 10);
	coverDiv.style.width = px(width);
	coverDiv.style.height = px((line.bottom - line.top) * scale);
	coverDiv.dataset.right = line.right * scale;
	coverDiv.dataset.width = width; 
	coverDiv.className = hilite ? "hiliteDiv" : "coverDiv";
	return coverDiv;
}
function px(val){
	return val.toString() + "px";
}
function measureLines(measurements){
	var prevBottom = -100;
	var prevTop = -100;
	var lines = []; 
	var lineData, word, prevRight;
	for (var i=0; i<measurements.length; i++){
		word = measurements[i];
		if (word.word === '') {
			continue;
		}
		if (word.bottom != prevBottom) {
			if (lineData) {
				lineData.right = prevRight;
				if (lineData.left != lineData.right){
					lines.push(lineData);
				}
			}
			lineData = { left:word.left, top:word.top, bottom:word.bottom, right:undefined};	
		}
		prevBottom = word.bottom;
		prevRight = word.right;
	}
	lineData.right = prevRight;
	if (lineData.left != lineData.right){
		lines.push(lineData);
	}
	return lines;
}
function reset(){
	tweet.style.transform = "";
	tweet.style.opacity = 0;
	bkg.style.opacity = 1;
	tweenData.scale = minScale;
}
function px(num){
	return num.toFixed(3) + 'px';
}
function updateTweenTime(time) {
	return new Promise( (resolve, reject) => {
		let pct = time / fullDuration;
		if (pct <= 0) pct = 0.001;
		tl.progress(pct);
		resolve();
	});
}
