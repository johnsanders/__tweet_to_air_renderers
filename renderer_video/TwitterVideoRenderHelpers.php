<?php 
	class TwitterVideoRenderHelpers {		
		public static function sendToMediasource($msNumber){
			$localDirectory = "/home/cnnitouch/www/apps/twitter_snapper/tmp/";
			$server = "lonvideodrop.turner.com";
			$user = "xfer2lonms2";
			$pw = "2smnol2refx";

			$conn_id = ftp_connect($server);
			if ( ftp_login($conn_id, $user, $pw) ) {
				if (ftp_put($conn_id, $msNumber . ".mxf", $localDirectory . $msNumber . ".mxf", FTP_BINARY)) {

				} else {
					exitWithError();
				}
			} else {
				exitWithError();
			}
		}


		public static function exitWithError(){
			echo "error";
			exit;
		}
		public static function renderMxf($tweetId, $msNumber){
		/*
			exec( '/usr/local/bin/ffmpeg -thread_queue_size 512 -i /home/cnnitouch/www/apps/twitter_snapper/tmp/' . $tweetId . '_%05d.png \
			-f lavfi -i anullsrc -r 29.97 -ar 48000 -s:v 1440x1080 -b:v 35000k -minrate 25000k -maxrate 35000k \
			-c:v mpeg2video -vf scale=1440x1080,setsar=4:3,setdar=16:9 -pix_fmt yuv420p -c:a pcm_s16le \
			-map 0:v -map 1:a:0:1 -map 1:a:0:2 -map 1:a:0:3 -map 1:a:0:4 -threads 4 -shortest -y /home/cnnitouch/www/apps/twitter_snapper/tmp/' . $msNumber . '.mxf');
		*/

			exec( '/usr/local/bin/ffmpeg -thread_queue_size 512 -i /home/cnnitouch/www/apps/twitter_snapper/tmp/' . $tweetId . '_%05d.png \
			-f lavfi -i anullsrc -ac 1 -r 29.97 -vcodec mpeg2video -s:v 1440x1080 -b:v 35000k -maxrate 35000k -minrate 35000k -bufsize 3835k \
			-vf scale=1440x1080,setsar=4:3,setdar=16:9 -flags ilme -top 1 -acodec pcm_s16le -ar 48000 -pix_fmt yuv422p \
			-map 0:v -map 1:a:0:1 -map 1:a:0:2 -map 1:a:0:3 -map 1:a:0:4 -threads 4 -shortest -y /home/cnnitouch/www/apps/twitter_snapper/tmp/' . $msNumber . '.mxf'  );
		}

		public static function sendEmail($twitterUser, $msNumber, $email){
			$to      = $email; 
			$subject = 'Your Tweet to Air render';
			$message = "Hi there.  Your animation of a tweet by @$twitterUser is now ready in Atlanta playback on MS# $msNumber.\n";
			$headers = 'From: john.sanders@turner.com' . "\r\n" .
						'Reply-To: john.sanders@turner.com' . "\r\n" .
						'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);
		}
		public static function doLog($twitterUser, $msNumber, $email){
			file_put_contents('/home/cnnitouch/touch.log', "Tweet to MS @$twitterUser $email $msNumber\n", FILE_APPEND);
		}

		public static function killXvfb($xvfbPid){
			exec( "kill $xvfbPid" );
		}
		public static function killSelenium($seleniumPid){
			exec( "kill $seleniumPid" );
		}
		public static function startXvfb($rendererNum){
			$displayNum = strval(100 - $rendererNum);
			exec ("/usr/bin/Xvfb :" . $displayNum . " -ac -screen 0 2560x1440x24 &> /dev/null & echo $!", $xvfbInfo);
			return $xvfbInfo[0];
		}
		public static function getXvfbPid($rendererNum){
			$displayNum = strval(100 - $rendererNum);
			exec( 'pgrep -f "Xvfb :' . $displayNum . '"', $out );
			if ( count($out) == 0 ){
				return "";
			} else {
				return $out[0];
			}
		}
		public static function getSeleniumPid($rendererNum){
			$displayNum = strval(100 - $rendererNum);
			exec( 'ps e -e | grep [j]ava | grep "DISPLAY=:' . $displayNum . '"', $out ); // SO SOMEHOW THOSE BRACKETS MAKE THE GREP NOT MATCH THE PS PROCESS ITSELF.
			
			if ( count($out) == 0 ){
				return "";
			}
			$arr = explode(' ', trim($out[0]));
			return $arr[0];
		}
		public static function startSelenium($rendererNum,$host,$tweetId,$dbLink){
			$displayNum = strval(100 - $rendererNum);
			$portNum = strval(4443 + $rendererNum);
			function seleniumReady($host){
				$thing = file_get_contents( $host . '/static/resource/hub.html' );
				var_dump($thing);
				return strlen($thing) > 0;
			}
			exec ("DISPLAY=:" . $displayNum . " /usr/bin/java -jar /home/cnnitouch/phantomjs/selenium-server-standalone-2.53.1.jar -port " . $portNum . " &> /dev/null & echo $!", $seleniumInfo);
			$waitCount = 0;
			while ( !seleniumReady($host) ){
				echo "Trying selenium.";
				$waitCount++;
				if ($waitCount > 300){
					updateProgress( $tweetId, 'Failed to start', $dbLink, $rendererNum, false );
					renderUnlock($dataFile);
					exit;
				}
				sleep(5);
			}
			return $seleniumInfo[0];
		}

		public static function countRenderedFiles($tweetId){
			return count( glob( "/home/cnnitouch/www/apps/twitter_snapper/tmp/${tweetId}_*" ) );
		}
		public static function okToKillProcesses($maxActive){
			$statuses = file_get_contents('http://localhost/apps/flashSQLinterface/read3.php?table=status_render&where=started%20>%20NOW()%20-%20INTERVAL%201%20DAY');
			$statuses = json_decode($statuses);
			$numActive = 0;
			foreach ($statuses as $status){
				if ($status->status != 'Finished'){
					$numActive++;
				}
			}
			return $numActive <= $maxActive;
		}

		public static function destroyAllEvidence($tweetId, $msNumber){
			foreach (glob("/home/cnnitouch/www/apps/twitter_snapper/tmp/$tweetId*") as $filename) {
				unlink($filename);
			}
			unlink("/home/cnnitouch/www/apps/twitter_snapper/tmp/$msNumber.mxf");
		}
		public static function renderLock($dataFile){
			file_put_contents( $dataFile, "1" );
		}
		public static function renderUnlock($dataFile){
			file_put_contents( $dataFile, "0" );
		}
		public static function sendToAtlantaPlayback($id) {
			file_get_contents( "http://localhost/utilities/bmam/sendToAtlantaPlayback.php?id=$id" );
		}
		public static function checkMsImport($id){
			$info = json_decode( file_get_contents( "http://localhost/utilities/bmam/getMsInfo.php?id=$id&loc=london"  )  );
			return $info->response->payload->items[0]->importStatus == "READY";
		}
		public static function checkAtlPlayback($id){
			$info = file_get_contents( "http://localhost/utilities/bmam/getMsInfo.php?id=$id&loc=atlanta" );
			$info = json_decode($info);
			$info = $info->response->payload->items[0];
			return $info->onPlayback == "true";
		}
	}
?>
