<?php 
require_once('/home/cnnitouch/www/utilities/vendor/autoload.php'); 
require("/home/cnnitouch/www/apps/twitter_snapper/helpers/TwitterDbHelpers.php");
require("/home/cnnitouch/www/apps/twitter_snapper/renderer_video/TwitterVideoRenderHelpers.php");

if (isSet($_GET['id'])) { 
	$tweetId = $_GET['id']; 
	$twitterUser = $_GET['twitterUser']; 
	$email = $_GET['email']; 
	$msNumber = $_GET['msNumber']; 
	$rendererNum = (int)$_GET['renderer']; 
} else { 
	$tweetId = $argv[1]; 
	$twitterUser = $argv[2]; 
	$email = $argv[3]; 
	$msNumber = $argv[4]; 
	$rendererNum = (int)$argv[5]; 
}
$dataFile = '/home/cnnitouch/www/apps/twitter_snapper/renderer_video/render_lock.dat';
$host = 'http://localhost:' . strval(4443 + $rendererNum) . '/wd/hub'; 
$dbLink = mysqli_connect('localhost', 'root', 'root', 'cnniwall'); 
if (!$dbLink) { 
	die('Could not connect: ' . mysql_error()); 
} 
TwitterVideoRenderHelpers::renderLock($dataFile);
TwitterDbHelpers::setStarted($tweetId, $dbLink); 
$xvfbPid = TwitterVideoRenderHelpers::getXvfbPid($rendererNum); 
if ( $xvfbPid == "" ) { 
	$xvfbPid = TwitterVideoRenderHelpers::startXvfb($rendererNum); 
}	
TwitterDbHelpers::updateProgress( $tweetId, 'Starting Selenium (0/4)', $dbLink, $rendererNum, false ); 
$seleniumPid = TwitterVideoRenderHelpers::getSeleniumPid($rendererNum); 
if ( $seleniumPid == "" ) { 
	$seleniumPid = TwitterVideoRenderHelpers::startSelenium($rendererNum,$host,$tweetId, $dbLink); 
} 
TwitterDbHelpers::incrementSeleniumReady( $tweetId, $dbLink, $rendererNum ); 
echo "$xvfbPid $seleniumPid"; sleep(2); 
TwitterDbHelpers::updateProgress( $tweetId, 'Launching Web Driver', $dbLink, $rendererNum, false); 
$capabilities = Facebook\WebDriver\Remote\DesiredCapabilities::firefox(); 
try {
	$driver = Facebook\WebDriver\Remote\RemoteWebDriver::create($host, $capabilities, 5000);
} catch (\Exception $e) {
	TwitterDbHelpers::updateProgress( $tweetId, 'Failed to Launch Driver', $dbLink, $rendererNum, false );
	var_dump($e);
	TwitterVideoRenderHelpers::killSelenium($seleniumPid);
	TwitterVideoRenderHelpers::killXvfb($xvfbPid);
	exit;
}

$driver->manage()->window()->setSize(new Facebook\WebDriver\WebDriverDimension(1920, 1080));
TwitterDbHelpers::updateProgress( $tweetId, 'Loading Template', $dbLink, $rendererNum, false );
$driver->get('http://localhost/apps/twitter_snapper/renderer_video/template_video.html');
sleep(1);
TwitterDbHelpers::updateProgress( $tweetId, 'Populating Template', $dbLink, $rendererNum, false );
$driver->executeScript("loadTemplateItems('$tweetId')");
sleep(2);
$totalFrames = 300;
$framesPerRenderer = 75;
$firstFrame = ($rendererNum - 1) * $framesPerRenderer + 1;
$lastFrame = $firstFrame + $framesPerRenderer - 1; 

for ($i=$firstFrame; $i<=$lastFrame; $i++){
	$pct = $i / $totalFrames;
	$fileNum = str_pad($i,5,'0',STR_PAD_LEFT);
	$driver->executeScript("setProgress($pct);");
	$driver->takeScreenshot("/home/cnnitouch/www/apps/twitter_snapper/tmp/${tweetId}_$fileNum.png");
	if ( $rendererNum == 1 && $i % 3 == 0 ){
		$framesRendered = TwitterVideoRenderHelpers::countRenderedFiles($tweetId);
		TwitterDbHelpers::updateProgress( $tweetId, number_format($framesRendered / $totalFrames, 2), $dbLink, $rendererNum, false );
	}
}
$driver->quit();
if ($rendererNum != 1){
	if ( okToKillProcesses(1) ){
		TwitterVideoRenderHelpers::killXvfb($xvfbPid);
		TwitterVideoRenderHelpers::killSelenium($seleniumPid);
	}
	exit;
}

// MASTER RENDERER WAITS FOR OTHER RENDERERS TO FINISH, WHEN WE HAVE 300 FILES
$safety = 0;
while ( $safety < 100 && $framesRendered < $totalFrames ) {
	$framesRendered = TwitterVideoRenderHelpers::countRenderedFiles($tweetId);
	TwitterDbHelpers::updateProgress( $tweetId, number_format($framesRendered / $totalFrames, 2), $dbLink, $rendererNum, false );
	sleep(3);
}



for ($i=301; $i<=1350; $i++){
	copy( '/home/cnnitouch/www/apps/twitter_snapper/tmp/' . $tweetId . '_00300.png', '/home/cnnitouch/www/apps/twitter_snapper/tmp/' . $tweetId . '_' . str_pad($i, 5, '0', STR_PAD_LEFT) . '.png' );
}

TwitterDbHelpers::updateProgress( $tweetId, 'Converting for MediaSource', $dbLink, $rendererNum, false );
TwitterVideoRenderHelpers::renderMxf($tweetId, $msNumber);
TwitterDbHelpers::updateProgress( $tweetId, 'Transferring to MediaSource', $dbLink, $rendererNum, false );
TwitterVideoRenderHelpers::sendToMediasource($msNumber);
TwitterVideoRenderHelpers::renderUnlock($dataFile);
TwitterDbHelpers::updateProgress( $tweetId, 'Waiting for MS Ingest', $dbLink, $rendererNum, false );
TwitterVideoRenderHelpers::killXvfb($xvfbPid);
TwitterVideoRenderHelpers::killSelenium($seleniumPid);
sleep(30);
while ( !TwitterVideoRenderHelpers::checkMsImport($msNumber) ) {
	sleep(30);
}
TwitterDbHelpers::updateProgress( $tweetId, 'Waiting for ATL Playback Xfer', $dbLink, $rendererNum, false );
TwitterVideoRenderHelpers::sendToAtlantaPlayback( $msNumber );
TwitterVideoRenderHelpers::sendEmail($twitterUser, $msNumber, "john.sanders@turner.com");
sleep(30);
$count = 0;
$transferComplete = TwitterVideoRenderHelpers::checkAtlPlayback($msNumber); 
while ( !$transferComplete && $count < 20 ) {
	sleep(30);
	$transferComplete = TwitterVideoRenderHelpers::checkAtlPlayback($msNumber); 
	$count++;
}

if ( $count <= 20 ) {
	TwitterDbHelpers::setFinished( $tweetId, $dbLink );
} else {
	TwitterDbHelpers::updateProgress( $tweetId, 'LON->ATL Xfer Failed', $dbLink, $rendererNum, false );
}

TwitterVideoRenderHelpers::sendEmail($twitterUser, $msNumber, $email);
TwitterVideoRenderHelpers::doLog($twitterUser, $msNumber, $email);
TwitterVideoRenderHelpers::destroyAllEvidence($tweetId, $msNumber);




exit;

