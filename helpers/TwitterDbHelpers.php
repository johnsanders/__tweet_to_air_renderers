<?php

	class TwitterDbHelpers {
		public static function updateProgress($id, $status, $dbLink, $rendererNum, $force){
			if ( $rendererNum == 1 || $force ) {
				$query = "UPDATE `cnniwall`.`status_render`
					 SET `status` = '$status'
					 WHERE `status_render`.`data1` = '$id';";
				$result = mysqli_query ( $dbLink, $query );
			}	
		}
		public static function setFinished($tweetId, $dbLink){
			$query = "UPDATE `cnniwall`.`status_render`
					 SET `finished` = NOW(), `status` = 'Finished'
					 WHERE `status_render`.`data1` = '$tweetId';";
			$result = mysqli_query ( $dbLink, $query );
			mysqli_close($dbLink);
		}
		public static function setStarted($tweetId, $dbLink){
			$query = "UPDATE `cnniwall`.`status_render`
					 SET `started` = NOW()
					 WHERE `status_render`.`data1` = '$tweetId';";
			$result = mysqli_query ( $dbLink, $query );
		}

		public static function incrementSeleniumReady( $tweetId, $dbLink, $rendererNum ) {
			$seleniumReadyStatus = file_get_contents('http://localhost/apps/flashSQLinterface/read3.php?table=status_render&where=data1=' . $tweetId);
			$seleniumReadyStatus = json_decode($seleniumReadyStatus)[0]->status;
			preg_match( '/Starting Selenium \(([0-4])\/4\)/', $seleniumReadyStatus, $matches );
			if ( count($matches) > 1 ){
				$currentVal = intval( $matches[1] );
				$currentVal++;
				$currentVal = strval($currentVal);
				self::updateProgress( $tweetId, "Starting Selenium ($currentVal/4)", $dbLink, $rendererNum, true );
			}
		}

	}


?>
